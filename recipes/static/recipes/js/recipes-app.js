(function(){
    var app = angular.module('recipesApp', []);

    app.controller('recipesListController', ['$http', function($http){
	var recipes = this;
	recipes.recipes = [];
	$http.get('/api/recipes/all/').success(function(data){
	    recipes.recipes = data;
	});
    }]);
})();
