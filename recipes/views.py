from django.shortcuts import render

from .models import Recipe


def list_recipes(request):
    return render(request, 'recipes_list.html')


def api_list_recipes(request):
    recipes = Recipe.objects_to_json()
    return render(
        request,
        'json_data.json',
        {'data': recipes},
        content_type='application/json'
    )

