import json

from django.db import models


class Recipe(models.Model):

    name = models.CharField(max_length=100)
    ingredients = models.TextField()
    directions = models.TextField()

    @classmethod
    def objects_to_json(cls):
        """ Return a json Recipe array """
        recipes = [recipe._as_dict() for recipe in Recipe.objects.all()]
        return json.dumps(recipes)

    def _as_dict(self):
        """ Converts a recipe into a dict. Useful for Recipe.objects_to_json """
        return {
            'name': self.name,
            'ingredients': self._text_to_list(self.ingredients),
            'directions': self._text_to_list(self.directions)
        }

    def _text_to_list(self, text):
        """ Converts a TextField into a list of lines """
        return [line.strip() for line in text.strip().split('\n')]

    def __str__(self):
        return self.name
